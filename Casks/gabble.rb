class Gabble < Cask
  url 'http://gabbleapp.com/Gabble.zip'
  homepage 'http://gabbleapp.com/'
  version ' 1.5.2'
  sha1 '75388bd0132224309777dd943063534851367271'
  link 'Gabble.app'
end
