class MysqlLion < Cask
  url 'http://dev.mysql.com/get/Downloads/MySQL-5.6/mysql-5.6.16-osx10.7-x86_64.dmg'
  homepage 'http://www.mysql.com/'
  version '5.6.16'
  sha1 '2b0e806e61e547339c4a2fb97fa2ba55e02bc77f'
  install 'mysql-5.6.16-osx10.7-x86_64.pkg'
  install 'MySQLStartupItem.pkg'
  prefpane 'MySQL.prefPane'

  before_uninstall do
    system %Q{/usr/bin/sudo -E -- /Library/StartupItems/MySQLCOM/MySQLCOM stop}
  end
  uninstall :pkgutil => 'com.mysql.*',
            :files => [
              '/usr/local/mysql-5.6.16-osx10.7-x86_64',
              '/usr/local/mysql',
              prefpanedir.join('MySQL.prefPane')
            ]
end
