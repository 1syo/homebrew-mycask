class Insomniax < Cask
  url 'https://www.macupdate.com/download/22211/InsomniaX-2.1.3.tgz'
  homepage 'https://www.macupdate.com/app/mac/22211/insomniax/'
  version ' 2.1.3'
  sha1 '76978e1044b9aa4d9b212d675f7ec2c87674c9e5'
  link 'InsomniaX.app'
end
